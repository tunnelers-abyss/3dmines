mines = {}
local world_seed1 = minetest.get_mapgen_params().seed % 65537
local world_seed2 = minetest.get_mapgen_params().seed % 65521
local world_seed3 = minetest.get_mapgen_params().seed % 65519

minetest.register_node("3dmines:field", {
	description = "Mine Field",
	tiles = {"3dmines_field.png"},
	paramtype = "light",
	sunlight_propagates = true,
	light_source = 16,
	groups = {dig_immediate = 2},
	on_dig = function(pos, node, digger)
		mines.unseal(pos, digger)
		return false
	end,
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		mines.mark(pos, clicker)
	end,
})

minetest.register_node("3dmines:marker1", {
	description = "Marked Field",
	tiles = {"3dmines_field.png^3dmines_marker1.png"},
	paramtype = "light",
	sunlight_propagates = true,
	light_source = 16,
	groups = {cracky = 3},
})

minetest.register_node("3dmines:explosion1", {
	description = "MineNotFoundException",
	tiles = {"3dmines_explosion1.png"},
	paramtype = "light",
	sunlight_propagates = true,
	light_source = 16,
	groups = {cracky = 3},
})

for i = 1, 26 do
	minetest.register_node("3dmines:"..i, {
		description = "Number "..i,
		tiles = {"3dmines_"..i..".png"},
		paramtype = "light",
		sunlight_propagates = true,
		drawtype = "torchlike",
		walkable = false,
		pointable = false,
		buildable_to = true,
		groups = {cracky = 3},
	})
end

local function modexp(x, y, m)
	local result = 1
	while y > 0 do
		if y % 2 == 1 then
			result = (result*x) % m
			y = y - 1
		else
			x = (x*x) % m
			y = y / 2
		end
	end
	return result
end

local function rand3d(x, y, z, seed)
	local rx, ry, rz
	if x > 0 then
		rx = modexp(68, x, 65537)
	else
		rx = modexp(56863, -x, 65537)
	end
	if y > 0 then
		ry = modexp(388, y, 65537)
	else
		ry = modexp(43072, -y, 65537)
	end
	if z > 0 then
		rz = modexp(148, z, 65537)
	else
		rz = modexp(4871, -z, 65537)
	end
	return (((seed*rx) % 65537) * ((ry*rz) % 65537)) % 65537
end

function mines.is_mine_at(pos)
	if pos.x < 3 and pos.x > -3 and pos.y < 3 and pos.y > -3 and pos.z < 3 and pos.z > -3 then return false end
	local r = rand3d(pos.x, pos.y, pos.z, world_seed1) % 100
	local d = rand3d(math.floor(pos.x/16), math.floor(pos.y/16), math.floor(pos.z/16), world_seed2) % 20
	return r < d+10
end

function mines.get_number_at(pos)
	nm = 0
	for x = -1, 1 do
		for y = -1, 1 do
			for z = -1, 1 do
				if mines.is_mine_at({x=pos.x+x, y=pos.y+y, z=pos.z+z}) then
					nm = nm + 1
				end
			end
		end
	end
	return nm
end

function mines.unseal_recursive(pos, depth)
	depth = depth or 0
	node = minetest.get_node_or_nil(pos)
	if depth > 1000 then return end
	if not node then return end
	if node.name ~= "3dmines:field" then return end
	nm = mines.get_number_at(pos)
	if nm == 0 then
		minetest.set_node(pos, {name="air"})
		for x = -1, 1 do
			for y = -1, 1 do
				for z = -1, 1 do
					if (x ~= 0) or (y ~= 0) or (z ~= 0) then
						mines.unseal_recursive({x=pos.x+x, y=pos.y+y, z=pos.z+z}, depth+1)
					end
				end
			end
		end
	else
		minetest.set_node(pos, {name="3dmines:"..nm})
	end
end

function mines.unseal(pos, player)
	if mines.is_mine_at(pos) then
		minetest.set_node(pos, {name="3dmines:explosion1"})
		mines.punish(pos, player)
	else
		mines.unseal_recursive(pos, 0)
	end
end

function mines.mark(pos, player)
	if mines.is_mine_at(pos) then
		minetest.set_node(pos, {name="3dmines:marker1"})
	else
		mines.unseal_recursive(pos, 0)
		mines.punish(pos, player)
	end
end

function mines.punish(pos, player)

end

minetest.register_on_joinplayer(function(player, last_login)
	minetest.after(1, function()
		player:set_properties({visual_size = {x=0.5, y=0.5, z=0.5}, collisionbox = {-0.2, 0.0, -0.2, 0.2, 0.7, 0.2}})
		player:set_eye_offset({x=0,y=-8,z=0}, {x=0,y=-8,z=0})
		player:override_day_night_ratio(1)
		local name = player:get_player_name()
		privs = minetest.get_player_privs(name)
		privs.fly = true
		privs.teleport = true
		minetest.set_player_privs(name, privs)
	end)
end)


local c_field = minetest.get_content_id("3dmines:field")

minetest.register_on_generated(function(minp, maxp, seed)
	--easy reference to commonly used values
	--local t1 = os.clock()
	local x1 = maxp.x
	local y1 = maxp.y
	local z1 = maxp.z
	local x0 = minp.x
	local y0 = minp.y
	local z0 = minp.z
		

	local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
	local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
	local data = vm:get_data()

	--mandatory values
	local sidelen = x1 - x0 + 1 --length of a mapblock
	local chulens = {x=sidelen, y=sidelen, z=sidelen} --table of chunk edges
	local chulens2D = {x=sidelen, y=sidelen, z=1}
	local minposxyz = {x=x0, y=y0, z=z0} --bottom corner
	local minposxz = {x=x0, y=z0} --2D bottom corner

	local nixyz = 1 --3D node index
	local nixz = 1 --2D node index
	local nixyz2 = 1 --second 3D index for second loop
	

	for z = z0, z1 do -- for each xy plane progressing northwards
		--increment indices
		nixyz = nixyz + 1

		for y = y0, y1 do -- for each x row progressing upwards	

			local vi = area:index(x0, y, z)
			for x = x0, x1 do -- for each node do
				data[vi] = c_field
				nixyz2 = nixyz2 + 1
				nixz = nixz + 1
				vi = vi + 1
			end
			nixz = nixz - sidelen --shift the 2D index back
		end
		nixz = nixz + sidelen --shift the 2D index up a layer
	end

	--send data back to voxelmanip
	vm:set_data(data)
	--calc lighting
	vm:set_lighting({day=0, night=0})
	vm:calc_lighting()
	--write it to world
	vm:write_to_map(data)

	--local chugent = math.ceil((os.clock() - t1) * 1000) --grab how long it took
	--print ("[caverealms] "..chugent.." ms") --tell people how long
end)

-- unseal the origin at the beginning
local function reveal_origin(attempts)
	if attempts > 1000 then return end
	minetest.after(1, function()
		pos = {x=0, y=0, z=0}
		node = minetest.get_node_or_nil(pos)
		if not node then
			reveal_origin(attempts+1)
			return
		end
		if node.name ~= "3dmines:field" then return end
		mines.unseal_recursive(pos, 0)
	end)
end
reveal_origin(0)

